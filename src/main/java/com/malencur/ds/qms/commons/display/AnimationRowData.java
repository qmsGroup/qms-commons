package com.malencur.ds.qms.commons.display;

import java.util.List;

/**
 * Created by forando on 25.06.15.<br/>
 * Represents all com.logosprog.display.animation metadata associated with a particular terminal row.
 */
public class AnimationRowData {

    private final String TAG;

    /**
     * Type of operation that specifies delete com.logosprog.display.animation
     */
    public static final int OPERATION_DELETE = 0;
    /**
     * Type of operation that specifies add com.logosprog.display.animation
     */
    public static final int OPERATION_ADD = 1;

    private int animation;

    private int terminalNumber;

    private int checkSum;

    private List<TerminalData> terminals;

    private int clientNumber;

    /**
     *
     * @param animation Type of requested com.logosprog.display.animation can be
     *              <ul>
     *                  <li>{@link AnimationRowData #OPERATION_DELETE}</li>
     *                  <li>{@link AnimationRowData #OPERATION_ADD}</li>
     *              </ul>
     * @param terminalNumber The number of a terminal
     * @param checkSum The value to verify that com.logosprog.display.animation located the rows correctly.
     * @param terminals The list of terminals to be used for table restoring if checkSum does not match.
     * @param clients The assigned to a terminal client number.
     * @throws NullPointerException If <b>com.logosprog.display.animation</b> = {@link #OPERATION_ADD} and the <b>clients</b> param is NULL.
     */
    public AnimationRowData(int animation, int terminalNumber, int checkSum, List<TerminalData> terminals, int... clients) throws NullPointerException {

        if (animation == OPERATION_ADD && clients == null) throw new NullPointerException(
                "clients argument is NULL");

        TAG = this.getClass().getSimpleName();

        this.animation = animation;
        this.checkSum = checkSum;
        this.clientNumber = (clients.length >0)? clients[0] : -1;
        this.terminalNumber = terminalNumber;
        this.terminals = terminals;
    }

    public int getAnimation() {
        return animation;
    }

    public void setAnimation(int animation) {
        this.animation = animation;
    }

    public int getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(int checkSum) {
        this.checkSum = checkSum;
    }

    public int getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(int clientNumber) {
        this.clientNumber = clientNumber;
    }

    public int getTerminalNumber() {
        return terminalNumber;
    }

    public void setTerminalNumber(int terminalNumber) {
        this.terminalNumber = terminalNumber;
    }

    public List<TerminalData> getTerminals() {
        return terminals;
    }

    public void setTerminals(List<TerminalData> terminals) {
        this.terminals = terminals;
    }
}
