package com.malencur.ds.qms.commons.socket;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by forando on 18.09.14.<br/>
 * Defines communication protocol between a terminal and a server.
 */
public class TerminalMessage extends SocketMessage implements Serializable {

    /*
    ======================STATES============================
     */
    /**
     * The terminal is in state to request next client.
     */
    public final static int REQUEST_CLIENT = 100;
    /**
     * The terminal is in state to accept an already
     * requested terminal.
     */
    public final static int ACCEPT_CLIENT = 101;
    /**
     * The socket is ready for use but has been timed out
     * for some reason.
     */
    public final static int TIME_OUT = 102;
    /*
    ====================END STATES==========================
     */

    /*
    =====================COMMANDS===========================
     */
    /**
     * This command is used to inform the server of what service
     * this terminal is assigned to.
     */
    public final static int DEFINE_SERVICE = 111;
    /**
     * Defines whether terminal is allowed to request a new client.<br>
     * if {@link #value} = 0 - it's allowed, if {@link #value} = 1 - it's not
     */
    public final static int HOLD_TERMINAL = 112;
    /*
    ====================END COMMANDS========================
     */

     /*
    =====================VALUES===========================
     */

    public final static int DISPLAY_AVAILABLE = 121;
    public final static int DISPLAY_UNAVAILABLE = 122;

    public final static int CLIENT_AVAILABLE = 123;
    public final static int CLIENT_UNAVAILABLE = 124;

    public final static int DISPLAY_AND_CLIENT_AVAILABLE = 125;
    /*
    ====================END COMMANDS========================
     */


    private static final long serialVersionUID = -2936338105405371206L;

    /**
     * A value for the {@link #operation} to be provided.
     */
    private int value;

    /**
     * @param terminal     The id index
     * @param operation    The command to be executed
     * @param value        The operation value
     * @param date         Current time stamp
     * @param transferable See {@link #transferable}
     */
    public TerminalMessage(int terminal, int operation, int value, Date date, boolean transferable) {
        super(terminal, operation, date, transferable);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
