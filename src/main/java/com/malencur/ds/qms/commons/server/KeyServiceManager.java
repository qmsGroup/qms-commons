package com.malencur.ds.qms.commons.server;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.malencur.ds.qms.commons.APP;
import com.malencur.file.FileFactory;

/**
 * Created by 1145 on 25.09.2017.
 */
public class KeyServiceManager extends FileFactory {
    public KeyServiceManager() {
        super("key.txt", APP.getRootDir(), APP.XML_FOLDER_NAME, null);
    }

    private static final Logger log = LoggerFactory.getLogger(KeyServiceManager.class);

    public KeyGen getKey(){
        KeyGen key = null;
        String filePath = getDestinationFilePath();
        try(
                FileInputStream fileInputStream = new FileInputStream(filePath);
                ObjectInputStream is = new ObjectInputStream(fileInputStream)
        ){
            key = (KeyGen) is.readObject();
        }catch (IOException | ClassNotFoundException ex){
            log.error(ex.getMessage(), ex.getCause());
        }
        return key;
    }

    public boolean setKey(KeyGen key){
        boolean result = false;

        try( ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(getDestinationFilePath())) ) {
            os.writeObject(key);
            result = true;
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex.getCause());
        }

        return result;
    }
}
