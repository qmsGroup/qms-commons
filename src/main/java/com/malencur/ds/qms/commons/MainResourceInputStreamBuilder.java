package com.malencur.ds.qms.commons;

import java.io.IOException;
import java.io.InputStream;

import com.malencur.file.InputStreamFactoryKt;

/**
 * Created by forando on 15.06.15.<br>
 *     Provides method to create {@link java.io.InputStream} objects
 *     from resource files that are in the same package as this class.
 */
public class MainResourceInputStreamBuilder {
    public InputStream build(String fileName) throws IOException {
        return InputStreamFactoryKt.inputStreamFromResource(this, fileName);
    }
}
