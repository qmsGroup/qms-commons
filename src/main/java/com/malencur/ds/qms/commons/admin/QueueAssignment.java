package com.malencur.ds.qms.commons.admin;

import java.io.Serializable;
import java.util.Date;

/**
 * This class represents an assignment to the particular queue
 */
public class QueueAssignment implements Serializable {


    private static final long serialVersionUID = -3657356818324994623L;
    /**
     * The index of a particular service queue
     */
    private final int queue;
    /**
     * 1 means high priority, 0 - low priority.
     */
    private final int priority;

    /**
     * The terminal the client with this assignment has been called.<br><br>
     * <b>Protected from OverWriting</b>
     */
    private int terminal = -1;

    /**
     * Indicates whether this assignment has already been served.<br><br>
     * <b>Protected from OverWriting</b>
     */
    private volatile boolean finished = false;

    /**
     * The time when this assignment was added to the queue.<br><br>
     * <b>Protected from OverWriting</b>
     */
    private Date registerTime;

    /**
     * The time when the client was called on this assignment.<br><br>
     * <b>Protected from OverWriting</b>
     */
    private Date calledTime;

    /**
     * The time when the client was served on this assignment.<br/>
     * Means this assignment finish time.<br><br>
     * <b>Protected from OverWriting</b>
     */
    private Date servedTime;

    /**
     *
     * @param priority See {@link #priority}
     * @param queue See {@link #queue}
     */
    public QueueAssignment(int priority, int queue) {
        if (priority>0){
            this.priority = 1;
        }else{
            this.priority = 0;
        }
        this.queue = queue;
    }

    /**
     *
     * @return See {@link #priority}
     */
    public int getPriority() {
        return priority;
    }

    /**
     *
     * @return See {@link #queue}
     */
    public int getQueueIndex() {
        return queue;
    }

    /**
     *
     * @param terminal See {@link #terminal}<br><br>
     * <b>Protected from OverWriting</b>
     */
    public void setTerminal(int terminal){
        if (this.terminal == -1) this.terminal = terminal;
    }

    /**
     *
     * @return See {@link #terminal}
     */
    public int getTerminal(){return terminal;}

    /**
     *
     * @return See {@link #registerTime}
     */
    public Date getRegisterTime() {
        if (registerTime != null) {
            return new Date(registerTime.getTime());
        }else{
            return null;
        }
    }

    /**
     * Sets {@link #registerTime}<br><br>
     * <b>Protected from OverWriting</b>
     */
    public void setRegisterTime() {
        if (registerTime == null)
            this.registerTime = new Date();
    }

    /**
     *
     * @return See {@link #calledTime}
     */
    public Date getAssignedTime() {
        if (calledTime != null) {
            return new Date(calledTime.getTime());
        }else{
            return null;
        }
    }

    /**
     * Sets {@link #calledTime}<br><br>
     * <b>Protected from OverWriting</b>
     */
    public void setAssignedTime() {
        if (calledTime == null)
            this.calledTime = new Date();
    }

    /**
     *
     * @return See {@link #servedTime}
     */
    public Date getFinishTime() {
        if (servedTime != null) {
            return new Date(servedTime.getTime());
        }else{
            return null;
        }
    }

    /**
     * Sets {@link #servedTime}<br><br>
     * <b>Protected from OverWriting</b>
     */
    private void setFinishTime() {
        if (servedTime == null)
            this.servedTime = new Date();
    }

    /**
     *
     * @return See {@link #finished}
     */
    public boolean isFinished(){
        return finished;
    }

    /**
     * Protected from OverWriting
     */
    public void finish(){
        setFinishTime();
        finished = true;
    }
}
