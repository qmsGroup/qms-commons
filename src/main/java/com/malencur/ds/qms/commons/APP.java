package com.malencur.ds.qms.commons;


import java.io.File;

/**
 * Created by forando on 10.04.14
 */
public class APP {

    /**
     * The name of the internal file where all application launch
     * settings are located.
     */
    public static final String APP_LAUNCH_FILE_NAME = "applaunch.properties";

    /**
     * The name of a custom created environment variable for this app.
     * <br>
     * Environment Variable is set via Operating System and is visible
     * for all applications.
     */
    public static final String ENV_VARIABLE_NAME = "VERSIYA";
    /**
     * System folder name where all files of this application have to be
     * located
     */
    public static final String SYSTEM_FOLDER_NAME = "versiya";
    /**
     * Folder name where all xml files of this application have to be
     * located
     */
    public static final String XML_FOLDER_NAME = "xml";
    /**
     * Folder name where all video files of this application have to be
     * located
     */
    public static final String VIDEO_FOLDER_NAME = "videos";
    /**
     * Folder name where all video files of this application have to be
     * located
     */
    public static final String AUDIO_FOLDER_NAME = "audios";
    /**
     * Folder name where all plugins of this application have to be
     * located
     */
    public static final String PLUGINS_FOLDER_NAME = "plugins";
    /**
     * The folder name of where all *.txt config files are to be
     * located by default
     */
    public static final String SETTING_DIR_NAME = "settings";

    public final static int MAX_TERMINAL_QUANTITY = 16;
    /**
     * The maximum rows that is possible to display on the screen
     */
    public final static int LEVEL_QUANTITY = 5;

    //System Commands:
    @Deprecated
    public final static int RESET_SYSTEM = 0;
    @Deprecated
    public final static int PRINTER_ERROR_ON = 1;
    @Deprecated
    public final static int PRINTER_ERROR_OFF = 2;
    @Deprecated
    public final static int PRINT_TICKET = 6;
    @Deprecated
    public final static int STOP_SERVICE = 3;
    @Deprecated
    public final static int TRIGGER_SERVICE = 5;

    /**
     * Used only between server and display to notify the latter
     * that PrinterMessage.RESET_SYSTEM has been sent from admin to the server.
     */
    public final static int RESET_SERVICE = 4;

    /*
    Server stuff
     */
    public static final int PORT = 1337;

    /**
     * This method provides directory where all files of this program have to be
     * located.<br>
     * If administrator provided environment variable {@link #ENV_VARIABLE_NAME}, then the path is
     * got from it's value,<br>
     *     otherwise the folder is set in user home directory.
     * @return Directory Path
     */
    public static String getRootDir(){
        String customPath = System.getenv(ENV_VARIABLE_NAME);
        if (null != customPath) {
            return customPath;
        }else{
            return System.getProperty("user.home") + File.separator + SYSTEM_FOLDER_NAME;
        }
    }

    /**
     * This method provides the path to this app root <b>subdirectory or file</b> of any nested deep.<br>
     * @param names All nested one in another folder names (with file name in the end, if provided).
     * @return Complete path to the root subdirectory/file.
     * @throws NullPointerException If <b>names</b> array does not contain any items.
     */
    public static String getPath(String... names) {
        if (names == null || names.length < 1) {
            throw new IllegalArgumentException("names parameter is null or empty");
        }
        StringBuilder path = new StringBuilder(APP.getRootDir());
        for (String name : names){
            path.append(File.separator).append(name);
        }
        return path.toString();
    }

    /**
     *
     * @return If TRUE - then all messages that are needed only for debugging will be printed.
     * @deprecated use logger instead
     */
    @Deprecated
    public static boolean printDebugMessages(){
        return new AppLaunchProperties().getLogDebugMessages();
    }

    /**
     *
     * @return If TRUE - then the system is in debug mode.
     * @deprecated Since logger supposed to be used instead of sys.out, we don't need this flag anymore.
     */
    @Deprecated
    public static boolean debugMode(){
        return new AppLaunchProperties().getDebugMode();
    }
}
