package com.malencur.ds.qms.commons.display;

/**
 * Created by forando on 23.06.15.<br/>
 * Class for sharing general com.logosprog.display.animation data.
 */
public class AnimationData {

    /**
     * An interval, normally used for moving object com.logosprog.display.animation
     */
    public static final int TIMER_EVENT_INTERVAL = 10;
    /**
     * An interval, normally used for showing ticker messages
     */
    public static final int TIMER_TICKER_EVENT_INTERVAL = 20;
    /**
     * An interval for switching visibility of some object (blinking effect)
     */
    public static final int TIMER_BLINKING_EVENT_INTERVAL = 500;
    /**
     * Time the notification panel will be visible on a screen
     */
    public static final int TIME_FOR_NOTIFICATION_PANEL = 3000;
}
