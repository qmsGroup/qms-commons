package com.malencur.ds.qms.commons.server;

import java.io.Serializable;
import java.util.Date;

/**
 * Bean to keep server data to validate startup
 * Created by 1145 on 25.09.2017.
 */
public class KeyGen implements Serializable {
    private static final long serialVersionUID = -9152664232697699329L;

    private Date start;
    private int days;

    public KeyGen(Date start, int days) {
        this.start = start;
        this.days = days;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
