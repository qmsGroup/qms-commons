package com.malencur.ds.qms.commons.socket;


import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.malencur.ds.qms.commons.display.TerminalData;

/**
 * Created by forando on 11.09.14.<br/>
 * The message to be passed between server and display.
 */
public class DisplayMessage extends SocketMessage implements Serializable {

    public static final int INIT_ROWS = 200;
    public static final int DELETE_ROW = 201;
    public static final int ADD_ROW = 202;
    public static final int CLEAR_ROW = 203;
    public static final int RESTORE_ROW = 204;


    public static final int ANIMATION_FINISHED = 211;
    private static final long serialVersionUID = 7189291502073699812L;

    /*public static final int RESET_SYSTEM = 210;
    public static final int PRINTER_ERROR_ON = 211;
    public static final int PRINTER_ERROR_OFF = 212;
    public static final int STOP_SERVICE = 213;
    public static final int RESET_SERVICE = 214;
    public static final int TRIGGER_SERVICE = 215;*/

    /**
     * We need them for INIT and, if checkSum proved to be wrong - for restore correct table.
     */
    public List<TerminalData> terminals;
    /**
     * The terminal to perform operation on (delete row / add row)
     */
    public TerminalData terminal;
    /**
     * CheckSum to verify correct terminal rows location after com.logosprog.display.animation.
     */
    private int checkSum;
    private int restOfClients;
    private int terminalQuantity;

    /**
     * @param id           The id index
     * @param operation    The command to be executed
     * @param date         Current time stamp
     * @param transferable See {@link #transferable}
     */
    public DisplayMessage(int id, int operation, TerminalData terminal, List<TerminalData> terminals, int checkSum,
                          int restOfClients, int terminalQuantity, Date date, boolean transferable) {
        super(id, operation, date, transferable);
        this.terminal = terminal;
        this.terminals = terminals;
        this.checkSum = checkSum;
        this.restOfClients = restOfClients;
        this.terminalQuantity = terminalQuantity;
    }

    public int getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(int checkSum) {
        this.checkSum = checkSum;
    }

    public int getRestOfClients() {
        return restOfClients;
    }

    public void setRestOfClients(int restOfClients) {
        this.restOfClients = restOfClients;
    }

    public int getTerminalQuantity() {
        return terminalQuantity;
    }

    public void setTerminalQuantity(int terminalQuantity) {
        this.terminalQuantity = terminalQuantity;
    }
}
