package com.malencur.ds.qms.commons.display;

import java.io.Serializable;

/**
 * Created by forando on 26.09.14.
 * Base class to be instantiated both on Display and Server sides
 */
public class TerminalData implements Serializable {
    /**
     * Means that a client has been accepted by a terminal.
     */
    public static final int ACCEPTED = 0;
    /**
     * Used on the Terminal side only.<br>
     * Defines period of time between terminal call to the server a new client request
     * and the server response for that call.
     */
    public static final int CALLING = 1;
    /**
     * Means that a terminal has requested a new client
     * and waiting for his arrival.
     */
    public static final int WAITING = 2;
    /**
     * The terminal is connected to a server but currently is in break state (for a lunch or coffee break)
     */
    public static final int BREAK = 3;
    private static final long serialVersionUID = 7232868207623135505L;

    public int terminalID;
    public int levelIndex;
    public int clientNumber;
    public int terminalNumber;
    public boolean visible;
    /**
     * On a Terminal side can be:
     * <ul>
     *     <li>{@link #ACCEPTED}</li>
     *     <li>{@link #CALLING}</li>
     *     <li>{@link #WAITING}</li>
     *     <li>{@link #BREAK}</li>
     * </ul>
     *
     * On a Server side can be:
     * <ul>
     *     <li>{@link #ACCEPTED}</li>
     *     <li>{@link #WAITING}</li>
     * </ul>
     */
    public int state;

    /**
     *
     * @param levelIndex
     * @param clientNumber
     * @param terminalNumber
     * @param visible
     * @param state See {@link #state}
     * @param terminalID
     */
    public TerminalData(int levelIndex, int clientNumber, int terminalNumber, boolean visible,
                        int state, int terminalID) {
        this.levelIndex = levelIndex;
        this.clientNumber = clientNumber;
        this.terminalNumber = terminalNumber;
        this.visible = visible;
        this.state = state;
        this.terminalID = terminalID;
    }
}
