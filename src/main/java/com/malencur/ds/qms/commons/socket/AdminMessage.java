package com.malencur.ds.qms.commons.socket;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by forando on 02.10.15.
 */
public class AdminMessage extends SocketMessage implements Serializable {

    public static final int REGISTER_NEW_CLIENT = 300;
    private static final long serialVersionUID = -43074878644371924L;
    public int val;


    /**
     * @param id           The id index
     * @param operation    The command to be executed
     * @param date         Current time stamp
     */
    public AdminMessage(int id, int operation, int val, Date date, boolean transferable) {
        super(id, operation, date, transferable);
        this.val = val;
    }
}
