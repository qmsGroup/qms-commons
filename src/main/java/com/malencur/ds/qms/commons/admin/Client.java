package com.malencur.ds.qms.commons.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by forando on 02.10.15.<br/>
 * This class represents client.<br/>
 * The objects of this class are added to Queue.<br><br>
 * <b>All fields are protected from OverWriting</b>
 *
 */
public class Client implements Serializable, Comparable<Client> {


    private static final long serialVersionUID = -3427350264045944491L;
    /**
     * the client number<br><br>
     * <b>Protected from OverWriting</b>
     */
    private int number = -1;
    /**
     * The list of all queues to which the client was assigned<br><br>
     * <b>All fields are protected from OverWriting</b>
     */
    private List<QueueAssignment> assignments;

    /**
     * Shows which {@link QueueAssignment} from
     * {@link #assignments} is currently in progress
     */
    private int currentAssignmentIndex = -1;

    private final ClientLock clientLock;

    /**
     * @param assignments See {@link #assignments}
     */
    public Client(ArrayList<QueueAssignment> assignments){
        this.assignments = assignments;
        this.clientLock = new ClientLock();
    }

    /**
     * Tries to switch to next queue assigned for this client.
     * @return TRUE if there is next queue assigned for this client
     */
    public boolean goToNextAssignment(){
        boolean hasNextAssignment = false;
        synchronized (clientLock){
            int nextIndex = currentAssignmentIndex + 1;
            if (nextIndex < assignments.size()){
                currentAssignmentIndex++;
                hasNextAssignment = true;
            }
        }
        return hasNextAssignment;
    }

    public void setClientNumber(int number){
        if (this.number == -1)
        this.number = number;
    }

    public int getClientNumber(){return number;}

    public int getQueueIndex(){
        if (currentAssignmentIndex > -1) {
            return assignments.get(currentAssignmentIndex).getQueueIndex();
        }else return -1;
    }

    public int getPriority(){
        if (currentAssignmentIndex > -1) {
            return assignments.get(currentAssignmentIndex).getPriority();
        }else{
            return -1;
        }
    }

    public List<QueueAssignment> getAssignments(){return assignments;}

    /**
     * Protected from OverWriting
     * @param terminal The terminal number the client is assigned in the current assignment session
     */
    public void setTerminal(int terminal){
        assignments.get(currentAssignmentIndex).setTerminal(terminal);
    }

    public int getTerminal(){
        return assignments.get(currentAssignmentIndex).getTerminal();
    }

    /**
     * Protected from OverWriting
     */
    public void setRegisterTime() {
        assignments.get(currentAssignmentIndex).setRegisterTime();
    }

    /**
     * Protected from OverWriting
     */
    public void setAssignedTime() {
        assignments.get(currentAssignmentIndex).setAssignedTime();
    }

    /**
     * Protected from OverWriting
     */
    public void finish(){
        assignments.get(currentAssignmentIndex).finish();
    }

    @Override
    public int compareTo(Client client2) {
        if (getPriority() < client2.getPriority()) return 1;
        if (getPriority() > client2.getPriority()) return -1;
        if (getClientNumber() > client2.getClientNumber()) return 1;
        if (getClientNumber() < client2.getClientNumber()) return -1;
        return 0;
    }

    private class ClientLock implements Serializable{

        private static final long serialVersionUID = 3487216045153154467L;
    }
}
