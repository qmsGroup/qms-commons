package com.malencur.ds.qms.commons.display;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by forando on 22.06.15.<br/>
 * This class wraps {@link Queue} object and adds
 * {@link TableAnimationQueue.TableAnimationQueueListener}
 * callbacks to it's functionality
 */
public class TableAnimationQueue {
    private final String TAG;
    /**
     * A key to get com.logosprog.display.animation value from given HashMap<String, Integer>
     */
    //public static final String KEY_ANIMATION = "com.logosprog.display.animation";
    /**
     * A key to get terminal number from given HashMap<String, Integer>
     */
    //public static final String KEY_TERMINAL = "terminal";
    /**
     * A key to get terminal number from given HashMap<String, Integer>
     */
    //public static final String KEY_CHECKSUM = "checksum";
    /**
     * A key to get client number from given HashMap<String, Integer>
     */
    //public static final String KEY_CLIENT = "client";

    /**
     * The queue of scheduled animations with all meta data.
     */
    private volatile Queue<AnimationRowData> queue;


    public TableAnimationQueue(){
        TAG = this.getClass().getSimpleName();
        queue = new LinkedList<>();
        listeners = new ArrayList<>();
    }

    /**
     * The same as {@link Queue#offer(Object)}. But in addition to that,
     * when before this method is called the queue was empty, it triggers
     * {@link TableAnimationQueue.TableAnimationQueueListener#onTableAnimationQueueInit()}
     * callback.
     * @param data Data to be used when the item is retrieved from the {@link #queue}
     * @return boolean whether or not the element has been added to the queue
     */
    public boolean offer(AnimationRowData data){
        int size = queue.size();
        boolean result = queue.offer(data);
        if (size == 0){
            for (TableAnimationQueueListener listener : listeners){
                listener.onTableAnimationQueueInit();
            }
        }
        return result;
    }

    /**
     * The same as {@link Queue#poll()}.
     * @return
     */
    public AnimationRowData poll(){
        AnimationRowData data;
        try {
            data = queue.poll();
            return data;
        }catch (NullPointerException ex){
            return null;
        }
    }

    public int getSize(){
        return queue.size();
    }

    private List<TableAnimationQueueListener> listeners;

    public void addTableAnimationQueueListener(TableAnimationQueueListener listener){
        listeners.add(listener);
    }

    public interface TableAnimationQueueListener{
        /**
         * This callback is called when, after being empty, the first element
         * is added to the {@link TableAnimationQueue}
         * object
         */
        void onTableAnimationQueueInit();
    }
}
