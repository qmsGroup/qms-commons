package com.malencur.ds.qms.commons.admin.plugins.statistics

interface QmsStatisticsDataProvider {
    fun requestClientStatistics(clientNumber: Int)
    fun getCenterInfo(): String
    fun getServiceNames(): List<String>
}