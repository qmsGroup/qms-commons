package com.malencur.ds.qms.commons.plugin

interface PluginInfoResolver {
    /**
     * Stringified JSON with all plugin relevant information.
     *
     * Consumer of this property should apply transformation String -> Map<String, String>
     * to get all available information.
     */
    fun getInfo(): String

    /**
     * Defines whether this plugin provides stopping and restarting functionality.
     *
     * If not, invoking [stop] and [restart] will have no effects.
     */
    val isToggleable: Boolean

    /**
     * Use this API only if [isToggleable] == TRUE
     *
     * Stops this service activity.
     * It may still be running bu with no side effect to the module that uses it.
     */
    fun stop()

    /**
     * Use this API only if [isToggleable] == TRUE
     *
     * In case this plugin has been stopped with [stop], use this API to restart it.
     */
    fun restart()

    /**
     * Defines whether this service main activity is in operation.
     *
     * @return TRUE - if it's in operation,
     * FALSE - if not, though the service itself may still be running
     * with no side effect to the module that uses it.
     */
    fun isActive(): Boolean
}