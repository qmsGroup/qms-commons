package com.malencur.ds.qms.commons.admin.plugins.raspberry.gpio

interface QmsRaspberryGpioService {
    fun initialize(gpioListener: GpioListener)
}