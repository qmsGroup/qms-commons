package com.malencur.ds.qms.commons.admin.plugins.printer.ticketfactory

interface QmsTicketFactory {
    fun createTicket(context: QmsTicketContext): QmsTicketPrintable
}