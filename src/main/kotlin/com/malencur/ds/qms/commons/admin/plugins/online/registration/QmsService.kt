package com.malencur.ds.qms.commons.admin.plugins.online.registration

interface QmsService{
    fun register(registration: QmsRegistration): Boolean
    fun requestClientDetails(clientNumber: Int)
}