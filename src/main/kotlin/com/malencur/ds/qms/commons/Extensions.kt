package com.malencur.ds.qms.commons

import java.sql.Timestamp
import java.time.LocalDateTime
import java.util.*

fun Date.toLocaleDateTime(): LocalDateTime = Timestamp(this.time).toLocalDateTime()

fun LocalDateTime.toDate(): Date = Timestamp.valueOf(this)