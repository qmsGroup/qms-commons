package com.malencur.ds.qms.commons.admin.plugins.online.registration

import javafx.scene.control.Menu
import tornadofx.*
import java.util.*

interface OnlineRegistrationService {
    fun clientRegistered(qmsNumber: Int, priority: Int)
    fun clientAssigned(qmsNumber: Int, serviceStarted: Date)
    fun clientServiceFinished(qmsNumber: Int, serviceEnded: Date, terminal: Int)
    fun clientDetailsResponseReceived(clientDetails: ClientDetails)
    fun scheduleRegistrations(qmsService: QmsService)
    fun dispose()

    fun bindToPrimaryView(primaryView: View, menu: Menu)
}