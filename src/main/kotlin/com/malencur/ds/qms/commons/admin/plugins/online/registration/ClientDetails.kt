package com.malencur.ds.qms.commons.admin.plugins.online.registration

data class ClientDetails(val clientNumber: Int, val assignments: List<QueueAssignmentInfo>)