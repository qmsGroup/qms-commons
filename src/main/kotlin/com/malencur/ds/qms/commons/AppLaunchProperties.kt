package com.malencur.ds.qms.commons

import com.malencur.file.properties.factory.InternalPropertiesFactory

class AppLaunchProperties: InternalPropertiesFactory(APP.APP_LAUNCH_FILE_NAME){

    val terminalNumerationUser by booleanProperty(default = false)
    val logDebugMessages by booleanProperty(default = false)
    val debugMode by booleanProperty(default = false)
}