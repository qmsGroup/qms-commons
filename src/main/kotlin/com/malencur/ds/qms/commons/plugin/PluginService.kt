package com.malencur.ds.qms.commons.plugin

import com.malencur.ds.qms.commons.APP
import java.io.File
import java.net.URLClassLoader

object PluginService {

    private val pluginFolderPath = APP.getRootDir() + File.separator + "plugins"

    val classLoader by lazy {
        val pluginsDir = File(pluginFolderPath)
        val urls = pluginsDir.listFiles()
                .filter { it.extension == "jar" }
                .map { it.toURI().toURL() }
                .toTypedArray()
        URLClassLoader(urls, Thread.currentThread().contextClassLoader)
    }

    init {
        val file = File(pluginFolderPath)
        if (!file.exists()) file.mkdirs()
    }

    /**
     * We need this function only for PluginService object initialization
     */
    fun ensureFolderCreated() = File(pluginFolderPath).exists()
}