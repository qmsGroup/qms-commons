package com.malencur.ds.qms.commons.admin.plugins.raspberry.gpio

interface GpioListener {
    fun onRegisterNewClient()
}