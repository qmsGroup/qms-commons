package com.malencur.ds.qms.commons.admin.plugins.printer.ticketfactory

import org.slf4j.LoggerFactory
import java.awt.Font
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.font.FontRenderContext
import java.awt.print.PageFormat
import java.awt.print.Printable
import java.text.SimpleDateFormat
import java.util.*

abstract class QmsTicketPrintable(val ticketArgsValidator: () -> Unit) : Printable {

    override fun print(g: Graphics?, pf: PageFormat?, pageIndex: Int): Int {
        if (pageIndex > 0) { /* We have only one page, and 'page' is zero-based */
            return Printable.NO_SUCH_PAGE
        }

        if (g == null) {
            log.error("Graphics is NULL. Ignoring 'print' job")
            return EXITED_WITH_ERROR
        }

        if (pf == null) {
            log.error("PageFormat is NULL. Ignoring 'print' job")
            return EXITED_WITH_ERROR
        }

        try {
            ticketArgsValidator()
        } catch (ex: QmsTicketException) {
            log.error(ex.message)
            return EXITED_WITH_ERROR
        }

        g.drawRect(pf.imageableX.toInt(), pf.imageableY.toInt(), pf.imageableWidth.toInt(), pf.imageableHeight.toInt())

        val g2d = g as Graphics2D
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
        g2d.translate(pf.imageableX, pf.imageableY)

        return printTicket(g, pf)
    }

    abstract fun printTicket(g: Graphics, pf: PageFormat): Int

    companion object {
        fun getTextWidth(text: String, font: Font): Int {
            return font.getStringBounds(
                    text,
                    FontRenderContext(font.transform, false, false)
            ).bounds.getWidth().toInt()
        }

        val dateString: String
            get() {
                val date = Date()
                val dt = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault())

                return dt.format(date)
            }

        private val log = LoggerFactory.getLogger("QmsTicketPrintable")

        const val EXITED_WITH_ERROR = 2
    }
}