package com.malencur.ds.qms.commons.admin.plugins.statistics

import com.malencur.ds.qms.commons.admin.dto.ClientInfo
import javafx.scene.control.Menu
import tornadofx.*
import java.util.*

interface QmsStatisticsService {
    fun registerStatisticsDataProvider(dataProvider: QmsStatisticsDataProvider)
    fun serviceRegistered(qmsNumber: Int, serviceIndex: Int, registered: Date)
    fun serviceStarted(qmsNumber: Int, serviceIndex: Int, terminal: Int, started: Date)
    fun serviceEnded(qmsNumber: Int, serviceIndex: Int, ended: Date)
    fun clientStatisticsResponseReceived(clientStatistics: ClientInfo)
    fun dispose()

    fun bindToPrimaryView(primaryView: View, menu: Menu)
}