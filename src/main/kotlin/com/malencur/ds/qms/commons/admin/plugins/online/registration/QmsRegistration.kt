package com.malencur.ds.qms.commons.admin.plugins.online.registration

data class QmsRegistration(
    val qmsNumber: Int,
    val serviceIndex: Int,
    val priority: Int = 1
)