package com.malencur.ds.qms.commons.admin.plugins.online.registration

data class QueueAssignmentInfo(
    val service: Int,
    val priority: Int,
    val terminal: String? = null,
    val registerTime: String? = null,
    val calledTime: String? = null,
    val servedTime: String? = null
)