package com.malencur.ds.qms.commons.socket

import java.io.Serializable
import java.util.*

/**
 * @param id           The id index
 * @param operation    The command to be executed
 * @param date         Current time stamp
 * @param transferable See {@link #transferable}
 */
class PrinterMessage(id: Int, operation: Int, var client: com.malencur.ds.qms.commons.admin.Client, var additionalVal: Int, date: Date,
                     transferable: Boolean) : SocketMessage(id, operation, date, transferable), Serializable {
    companion object {
        val PRINT_TICKET = 0
        /**
         * Means Client is already assigned to a particular terminal
         */
        val CLIENT_ASSIGNED = 1
        /**
         * Means Client registered in the system but not assigned yet
         */
        val CLIENT_REGISTERED = 2
        /**
         * We cannot serve clients without any screen on
         */
        val DISPLAY_UNAVAILABLE = 11
        /**
         * With this we can serve clients now
         */
        val DISPLAY_AVAILABLE = 12
        /**
         * Administrator has to inform the client that he has to
         * come to particular terminal immediately
         */
        val SHOW_CLIENT_ASSIGNED_MESSAGE = 21
        val CLIENT_DETAILS_REQUEST = 22

        val RESET_SYSTEM = 31

        private val serialVersionUID = -1566464237311116774L
    }
}
