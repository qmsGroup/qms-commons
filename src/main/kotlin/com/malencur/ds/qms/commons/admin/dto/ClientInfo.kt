package com.malencur.ds.qms.commons.admin.dto

data class ClientInfo(val clientNumber: Int, val services: List<ClientServiceInfo>)