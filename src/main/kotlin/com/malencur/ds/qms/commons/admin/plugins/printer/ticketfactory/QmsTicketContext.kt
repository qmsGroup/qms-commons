package com.malencur.ds.qms.commons.admin.plugins.printer.ticketfactory

data class QmsTicketContext(
        val clientNumber: Int,
        val assignments: List<String>,
        val time: String,
        val priority: Boolean = false
)