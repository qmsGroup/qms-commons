package com.malencur.ds.qms.commons

import com.malencur.file.properties.factory.ExternalPropertiesFromTemplateFileFactory

/**
 * Created by forando on 27.05.15.<br>
 * This class creates external .txt settings file for all client side applications.<br>
 *     It accesses and changes settings that are common for all clients,
 *     such as those to connect to the server.
 */
abstract class ClientSettingsBuilder(fileName: String): ExternalPropertiesFromTemplateFileFactory(fileName, APP.getRootDir(), APP.SETTING_DIR_NAME){

    val serverIP by stringProperty()
    val serverPort by intProperty()
}