package com.malencur.ds.qms.commons.admin.dto

import java.time.LocalDateTime

data class ClientServiceInfo(
        val service: Int,
        val priority: Int,
        val terminal: String? = null,
        val registerTime: LocalDateTime? = null,
        val calledTime: LocalDateTime? = null,
        val servedTime: LocalDateTime? = null
)