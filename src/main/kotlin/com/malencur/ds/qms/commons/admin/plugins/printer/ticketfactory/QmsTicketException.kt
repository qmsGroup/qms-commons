package com.malencur.ds.qms.commons.admin.plugins.printer.ticketfactory

class QmsTicketException(message: String) : RuntimeException(message)