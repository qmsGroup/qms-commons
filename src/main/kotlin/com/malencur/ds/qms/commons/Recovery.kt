package com.malencur.ds.qms.commons

import com.malencur.file.FileFactory
import org.slf4j.LoggerFactory
import java.io.File
import java.util.*

internal fun deleteBrokenDataFile(fileName: String) {
    if (FileFactory.deleteFile(APP.getRootDir() + File.separator + APP.XML_FOLDER_NAME + File.separator + fileName)) {
        val log = LoggerFactory.getLogger("Recovery")
        log.error("file $fileName is broken and was deleted and replaced for a new one.")
    } else {
        val scanner = Scanner(System.`in`)
        println("==============================================================")
        println("file $fileName is broken. The server is going to be shut down.")
        println("==============================================================")

        println("Enter any key:")
        scanner.nextLine()

        /*Shut down the JVM*/
        System.exit(0)
    }
}