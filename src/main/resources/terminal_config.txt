
# If 1 the panel will be moved to left screen side and always on the top, if 0 - not
snapped=1

# Change this to appropriate terminal number. Must be > 0
terminal=1

# The server IP address
#serverIP=192.168.1.186
serverIP=localhost

# The server port this terminal can be connected to
serverPort=1337

# The service number (from 0 to 4)
service=0